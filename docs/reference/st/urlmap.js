baseURLs = [
    [ 'GLib', 'https://docs.gtk.org/glib/' ],
    [ 'GObject', 'https://docs.gtk.org/gobject/' ],
    [ 'Gio', 'https://docs.gtk.org/gio/' ],
    [ 'Pango', 'https://docs.gtk.org/pango/' ],
    [ 'GdkPixbuf', 'https://docs.gtk.org/gdk-pixbuf/' ],
    [ 'Clutter', 'https://gnome.pages.gitlab.gnome.org/mutter/clutter/' ],
    [ 'Cogl', 'https://gnome.pages.gitlab.gnome.org/mutter/cogl/' ],
    [ 'Meta', 'https://gnome.pages.gitlab.gnome.org/mutter/meta/' ],
]
